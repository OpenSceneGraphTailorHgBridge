#!/usr/bin/perl

# Very quick and dirty script to deal with missing .svn/*/tmp/entries files.
# Both tailor and svn cleanup fail.
# This script will attempt to create the missing tmp directories and create an
# empty entries file to make the process happy.
# This script is meant to exist in the root OSG directory and be run from there.

START:
$output = `svn cleanup 2>&1`;
print "Output: $output";
if( $output =~ m/Can't open file '(.*?)\/(\w+)': No such file or directory/ )
{
	print "dir: $1, $2";
	`mkdir \"$1\"`;
	$concat = $1 + $2;
	
	`touch \"$concat\"`;
	goto START;
}
elsif( $output =~ m/Can't open directory '(.*?)': No such file or directory/ )
{
	print "dir2: $1";
	`mkdir \"$1\"`;
	goto START;
}



